-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2020 a las 19:22:23
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inmobiliaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE `administradores` (
  `Id` int(1) NOT NULL,
  `Nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `Contrasena` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`Id`, `Nombre`, `Contrasena`) VALUES
(1, 'pepe', 'pepe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casas`
--

CREATE TABLE `casas` (
  `Id` int(11) NOT NULL,
  `Titulo` varchar(20) NOT NULL,
  `Habitaciones` int(11) NOT NULL,
  `Precio` int(11) NOT NULL,
  `Descripcion` text NOT NULL,
  `Distancia` float NOT NULL,
  `Telefono` int(9) NOT NULL,
  `Imagenes` varchar(300) NOT NULL,
  `Imagenes2` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `casas`
--

INSERT INTO `casas` (`Id`, `Titulo`, `Habitaciones`, `Precio`, `Descripcion`, `Distancia`, `Telefono`, `Imagenes`, `Imagenes2`) VALUES
(1, 'Monzalbarba', 4, 250000, 'Amplia casa rústica de 540 m² de superficie y 25.000 m² de parcela en Garrapinillos, Zaragoza', 3, 647566779, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/monzalbarba.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/monzalbarba.jpg'),
(2, 'Predicadores', 2, 120000, 'Casa en pleno centro de Zaragoza de 358m²', 3, 646352340, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/predicadores.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/predicadores.jpg'),
(3, 'Actur', 3, 119999, 'Bonito terreno rustico de 666m2 con casa y piscina en Zaragoza', 6, 765867567, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/Actur.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/Actur.jpg'),
(4, 'Casetas', 8, 250000, 'Vivienda unifamiliar con piscina en finca rústica en Casetas(ZARAGOZA).', 5, 645129789, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/casetas.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/casetas.jpg'),
(5, 'Fuentes', 2, 50000, 'Casa en Las Fuentes, a 3.45 km del Colegio Montessoru, barrio tranquilo. ', 3, 678569458, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/fuentes.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/fuentes.jpg'),
(6, 'Universidad San Fran', 2, 130000, 'Piso en zona universitaria. 96 metros construidos y 89 metros útiles, para reformar.', 1, 465347568, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/sanfrancisco.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/sanfrancisco.jpg'),
(7, 'Parque del Agua', 10, 50000, 'Casa con grandes vistas a la Expo', 4, 566768798, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/parqueagua.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/parqueagua.jpg'),
(8, 'Almozara', 3, 500345, 'Dotada de todos los servicios, comercios, restauración, iglesias y transporte público. ', 5, 678678798, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/almozara.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/almozara.jpg'),
(9, 'Murallas', 4, 345879, 'Casa o chalet independiente en venta en Murallas', 3, 678678567, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/murallas.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/murallas.jpg'),
(10, 'Romareda', 6, 400000, 'Es una casa, construida en el año 2007, que tiene 300 m2 y dispone de 5 habitaciones y 5 baños. El suelo es de tarima.', 7, 678798679, '../../usuarioadministrador/gestiones/gestionescasas/añadircasa/romareda.jpg', 'usuarioadministrador/gestiones/gestionescasas/añadircasa/romareda.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casasfavoritas`
--

CREATE TABLE `casasfavoritas` (
  `Id` int(1) NOT NULL,
  `IdCasa` int(1) NOT NULL,
  `IdUsuario` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajeria`
--

CREATE TABLE `mensajeria` (
  `Id` int(11) NOT NULL,
  `Usuario` varchar(20) NOT NULL,
  `MensajeUsuario` varchar(100) NOT NULL,
  `MensajeAdministrador` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisos`
--

CREATE TABLE `pisos` (
  `Id` int(1) NOT NULL,
  `Titulo` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `Habitaciones` int(11) NOT NULL,
  `Precio` double NOT NULL,
  `Descripcion` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `Distancia` float NOT NULL,
  `Telefono` int(9) NOT NULL,
  `Imagenes` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `Imagenes2` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pisos`
--

INSERT INTO `pisos` (`Id`, `Titulo`, `Habitaciones`, `Precio`, `Descripcion`, `Distancia`, `Telefono`, `Imagenes`, `Imagenes2`) VALUES
(1, 'Universidad San Fran', 3, 120000, 'Piso zona universitaria, son 96 metros construidos y 89 metros útiles, para reformar.', 1, 546567879, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/sanfrancisco.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/sanfrancisco.jpg'),
(2, 'San Jose', 3, 70000, 'Es un piso que tiene 52 m2 y dispone de 2 habitaciones y 1 baños.', 3, 678564567, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/sanjose.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/sanjose.jpg'),
(3, 'Paseo Independencia', 5, 685000, 'Piso, ubicado en Paseo Independencia con varios servicios, comercios, restauración, transporte publico.', 1, 678675659, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/independencia.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/independencia.jpg'),
(4, 'Plaza de Toros', 8, 311000, 'Precioso ático a 3 minutos de Plaza España, magnífica orientación, muy soleado. ', 3, 678675687, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/plazadetoros.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/plazadetoros.jpg'),
(5, 'Predicadores', 4, 60000, 'Apartamento ideal para una Pareja o Single, en pleno Casco Histórico, calle Predicadores y Álvarez Casta', 3, 678656799, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/predicadores.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/predicadores.jpg'),
(6, 'Almozara', 2, 50405, 'Piso con 96 metros construidos y 89 metros útiles, para reformar.', 4, 678678799, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/almozara.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/almozara.jpg'),
(7, 'San Jose', 5, 10203, 'Dos amplios dormitorios dobles de 19 m2 y 15m2 exteriores. Una gran cocina 10m2 con galería de 2,60m2, y dos cuartos de baños completos', 4, 678678798, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/sanjosepiso.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/sanjosepiso.jpg'),
(8, 'Cesar Augusto', 2, 30456, 'El piso cuenta con un hall y dos dormitorios de 11,10m2 y otro de 7,30 m2 exteriores', 2, 678675687, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/cesaraugusto.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/cesaraugusto.jpg'),
(9, 'Puerto Venecia', 5, 500500, 'Es un piso, construido en el año 2003, que tiene 100 m2 de los cuales 92 m2 son útiles y dispone de 3 habitaciones y 2 baños.', 6, 768678697, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/puertovenecia.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/puertovenecia.jpg'),
(10, 'Torrero', 8, 405677, 'Es un piso, construido en el año 1963, que tiene 82 m2 de los cuales 82 m2 son útiles', 7, 768793845, '../../usuarioadministrador/gestiones/gestionespisos/añadirpiso/torrero.jpg', 'usuarioadministrador/gestiones/gestionespisos/añadirpiso/torrero.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisosfavoritos`
--

CREATE TABLE `pisosfavoritos` (
  `Id` int(1) NOT NULL,
  `IdPiso` int(1) NOT NULL,
  `IdUsuario` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(1) NOT NULL,
  `Nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `Contrasena` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `Apellido1` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `Apellido2` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `Edad` int(2) NOT NULL,
  `Telefono` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `casas`
--
ALTER TABLE `casas`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `casasfavoritas`
--
ALTER TABLE `casasfavoritas`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `mensajeria`
--
ALTER TABLE `mensajeria`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `pisosfavoritos`
--
ALTER TABLE `pisosfavoritos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores`
--
ALTER TABLE `administradores`
  MODIFY `Id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `casas`
--
ALTER TABLE `casas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `casasfavoritas`
--
ALTER TABLE `casasfavoritas`
  MODIFY `Id` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mensajeria`
--
ALTER TABLE `mensajeria`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pisos`
--
ALTER TABLE `pisos`
  MODIFY `Id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `pisosfavoritos`
--
ALTER TABLE `pisosfavoritos`
  MODIFY `Id` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id` int(1) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
